#Install prerequisites
sudo apt install apt-transport-https ca-certificates curl software-properties-common
#Add docker gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#Add docker repo to apt sources
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
#Make sure about to install from docker repo not ubuntu
#apt-cache policy docker-ce
#install docker
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

