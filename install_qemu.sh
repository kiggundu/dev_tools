#Install QEMU
sudo apt-get update && sudo apt upgrade
sudo apt-get install -y qemu
sudo apt-get install -y qemu-kvm
sudo apt-get install -y libvirt-daemon
sudo apt-get install -y qemu-system
sudo apt-get install -y qemu-utils
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
sudo apt-get install -y bridge-utils
sudo apt-get install -y virtinst
sudo apt-get install -y libvirt-daemon-system
sudo apt-get install -y virt-manager
sudo apt-get install -y virt-viewer
#sudo apt-get install -y libvirt-bin
sudo apt-get install -y libvirt-clients
sudo apt-get install -y qemu-system-arm
sudo apt-get install -y qemu-efi
#start and enable libvirtd virtualisation demon
sudo systemctl enable --now libvirtd
sudo systemctl start libvirtd
#sudo systemctl status libvirtd

#Add logged in user to the libvirt and kvm groups
sudo usermod -aG kvm $USER
sudo usermod -aG libvirt $USER

