#curl -O https://dl.google.com/go/go1.10.3.linux-amd64.tar.gz
#sha256sum go1.10.3.linux-amd64.tar.gz
#tar xvf go1.10.3.linux-amd64.tar.gz
#sudo chown -R root:root ./go
#sudo mv go /usr/local

#sudo apt insall -y golang-go #version of go installed was too old for fabric

curl -O https://go.dev/dl/go1.23.3.linux-amd64.tar.gz
shar256sum go1.23.3.linux-amd64.tar.gz
mkdir -P ~/workspace/go/temp
mv go1.23.3.linux-amd64.tar.gz ~/workspace/go/temp/
cd ~/workspace/go/temp
tar xvf go1.23.3.linux-amd64.tar.gz
mv go ../go1.23.3
cd ..
rm -rf go temp
ln -s go1.23.3 go
