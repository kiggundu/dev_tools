
# These prerequisites are required when installing qutebrowser from qithub
# apt install --no-install-recommends git ca-certificates python3 python3-venv libgl1 libxkbcommon-x11-0 libegl1-mesa libfontconfig1 libglib2.0-0 libdbus-1-3 libxcb-cursor0 libxcb-icccm4 libxcb-keysyms1 libxcb-shape0 libnss3 libxcomposite1 libxdamage1 libxrender1 libxrandr2 libxtst6 libxi6 libasound2

sudo apt install -y qutebrowser
git clone https://github.com/dracula/qutebrowser-dracula-theme.git ~/.config/qutebrowser/dracula

cat <<EOT >> ~/.config/qutebrowser/config.py
import dracula.draw

# Load existing settings made via :set
config.load_autoconfig()

dracula.draw.blood(c, {
    'spacing': {
        'vertical': 6,
        'horizontal': 8
    }
})
EOT

mv ~/.config/qutebrowser ~/.config/qutebrowser-backup
ln -s ~/.dotfiles/config/qutebrowser ~/.config/qutebrowser

mkdir -p ~/.local/share/qutebrowser
ln -s ~/.config/qutebrowser/greasemonkey ~/.local/share/qutebrowser/greasemonkey
