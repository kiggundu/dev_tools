#pipeable TTS solution
pipx install piper-tts

#spchcat STT solution
aria2c https://github.com/petewarden/spchcat/releases/download/v0.0.2-alpha/spchcat_0.0-2_amd64.deb
dpkg -i spchcat_0.0-2_amd64.deb
sudo dpkg -i spchcat_0.0-2_amd64.deb

#ollama
curl -fsSL https://ollama.com/install.sh | sh
ollama pull llama3.2
