curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

nvm install lts/hydrogen
sudo apt install -y npm
sudo npm install -g yarn

# Install instantmarkdfown used by vim to preview markdown files
sudo npm -g install instant-markdown-d

