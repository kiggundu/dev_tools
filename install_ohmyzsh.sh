sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

mv ~/.oh-my-zsh/custom/profiles ~/.oh-my-zsh/custom/profiles
ln -s ~/.dotfiles/.oh-my-zsh/custom/profiles ~/.oh-my-zsh/custom/profiles

mv ~/.zshrc  ~/.zshrc-backup
ln -s ~/.dotfiles/.zshrc ~/.zshrc

mv ~/.profile  ~/.profile-backup
ln -s ~/.dotfiles/.profile ~/.profile

