gcl git@kiggys.gitlab.com:kiggundu/gpt4all ~/workspace/gpt4all
mkdir ~/gpt4all-cli
ln -s ~/workspace/gpt4all/gpt4all-bindings/cli/app.py ~/gpt4all-cli/app.py

cat <<EOT >> ~/.profile
alias gptai="python3 ~/gpt4all-cli/app.py repl --model ~/local-llm-models/defaut.gguf"
alias gptaiu="python3 ~/gpt4all-cli/app.py repl --model ~/local-llm-models/defaut_uncensored.gguf"
EOT
