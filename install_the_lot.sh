./install_apt_tools.sh
./install_tor_services.sh
./install-tor-browser.sh
./install_my_dotfiles.zsh

#installs below here can clone git repos
./install-gdu.sh
./install_ohmyzsh.sh
./install_youtube_dl.sh
./install_jenv.sh
./install_pyenv.sh
./install_sdkman.sh
./install_fzf_linux.sh
./install_vimruntime.sh
./install_qutebrowser.sh
./install_gnuradio.sh
./install_variety_desktop.sh

./install_thunderbird.sh

./install_gpt4all.sh
./install-truecrypt.sh
