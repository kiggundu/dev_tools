
#Install OSX VM
sudo apt install -y libpixman-1-dev
sudo apt install -y libcairo2-dev
sudo apt install -y libpango1.0-dev
sudo apt install -y libjpeg8-dev
sudo apt install -y libgif-dev
sudo apt install -y ninja-build

git clone https://github.com/foxlet/macOS-Simple-KVM.git
cd macOS-Simple-KVM
./jumpstart.sh --catalina

qemu-img create -f qcow2 MyDisk.qcow2 64G
echo <<EOT >> basic.sh
-drive id=SystemDisk,if=none,file=MyDisk.qcow2 \\
-device ide-hd,bus=sata.4,drive=SystemDisk \\
EOT

./basic.sh
