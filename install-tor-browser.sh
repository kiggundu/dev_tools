wget https://dist.torproject.org/torbrowser/13.5/tor-browser-linux-x86_64-13.5.tar.xz
mv tor-browser-linux-x86_64-13.5.tar.xz tor-browser.tar.xz
7z x tor-browser.tar.xz
tar xf tor-browser.tar
mv tor-browser ~/.local/share/
cd ~/.local/share/tor-browser
./start-tor-browser.desktop --register-app
