git clone git@kiggys.gitlab.com:kiggundu/dotfiles ~/.dotfiles

mv ~/.config/git  ~/.config/git-backup
ln -s ~/.dotfiles/config/git ~/.config/git

mv ~/.byobu  ~/.byobu-backup
ln -s ~/.dotfiles/.byobu ~/.byobu

mv ~/.ssh/config  ~/.ssh/config-backup
ln -s ~/.dotfiles/.ssh/config ~/.ssh/config
